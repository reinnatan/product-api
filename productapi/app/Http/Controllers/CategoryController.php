<?php

namespace App\Http\Controllers;
use App\Product;
use DB;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
   
    public function __construct()
    {}
    
    public function showCategories(){
         $genquery = " SELECT id, name FROM Category";
         $data = DB::select($genquery);
        return response($data)->header('Content-Type', 'application/json');
    }

    
}

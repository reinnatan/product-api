<?php

namespace App\Http\Controllers;
use App\Product;
use DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{
   
    public function __construct()
    {}

    public function showProduct(Request $request){
        $genquery = " SELECT p.id, p.name, p.price, c.name as category FROM product p join category c on p.category=c.id";
        
        if ($request->get('name')){
            $nama = $request->get('name');
            $genquery = $genquery." where p.name='$nama'";
        }
        
        if ($request->get('catid')){
            $catid = $request->get('catid');
            if ($request->get('name')){
                $genquery = $genquery." and p.category=$catid";
                //echo $genquery;
            }else{
                $genquery = $genquery." where p.category=$catid";
            }
        }
        
        $data = DB::select($genquery);
        return response($data)->header('Content-Type', 'application/json');
    }
    
    public function showProductById($id){
        $data = DB::select("
            SELECT p.id, p.name, p.price, c.name as category FROM product p join category c on p.category=c.id where p.id=$id");
        return response($data)->header('Content-Type', 'application/json');
    }
    
}
